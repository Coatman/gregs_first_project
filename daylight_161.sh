#! /bin/bash

# Compute    sunrise, sunset, and daylight
# Input  1:  address, city, state, zip
# Input  2:  start date, stop date, directory name, file name
# Input  3:  timezone difference, in integer hours, as compared to eastern time zone
# Output 1: .txt file with sunrise, sunset, and daylight as YYYY-mm-dd HH:MM:SS
# Output 2: .r   file to pass as input to R to graph sunrise, sunset, and daylight
# Output 3: .pdf file of graph created by R

# Instead of using
# cat  template_labels_04.r >> ${DIRE}/${FILE}.r
# now calls
# cat  template_labels_05.r >> ${DIRE}/${FILE}.r
# Added Applied
# Added BAE
# Added Concept Solutions
# Added SEC
# Added Fannie Mae
# 2018-02-14 Greg Coats

NUM_CLA="9"
if [ $# != ${NUM_CLA} ]
then
  echo "The NUMber of Command Line Arguments must be ${NUM_CLA}, but is $#"
  echo "To use this script, enter ${NUM_CLA} command line arguments"
  echo "For example"
  echo "$0 <STREETADDRESS> <CITY> <STATE> <ZIP> <STAR> <STOP> <DIRE> <FILE> <TIMEZONEADJUST>"
  echo "$0 \"12201 Sunrise Valley Drive\"      Reston       VA 20191  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/usgs         USGS      0"
  echo "$0 \"15049 Conference Center Drive\" Chantilly    VA 20151  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/aerospace_va Aerospace 0"
  echo "$0 \"2310 East El Segundo Boulevard\" \"El Segundo\" CA 90245  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/aerospace_ca Aerospace 3"
  echo "$0 \"2300 Corporate Park Drive\"       Herndon      VA 20171  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/megapath_va  MegaPath  0"
  echo "$0 \"6800 Koll Center Parkway\"        Pleasanton   CA 94566  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/megapath_ca  MegaPath  3"
  echo "$0 \"8330 Boone Boulevard\"            Vienna       VA 22182  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/dublabs      DubLabs   0"
  echo "$0 \"436 Strasburg Avenue\"            Parkesburg   PA 19365  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/allvend      AllVend   0"
  echo "$0 \"11921 Freedom Drive\"             Reston       VA 20190  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/longview     LongView  0"
  echo "$0 \"11955 Democracy Drive\"           Reston       VA 20190  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/college      \"College Board\" 0"
  echo "$0 \"3 Glengyle Lane\"                 Sterling     VA 20165  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/donato       Donato    0"
  echo "$0 \"12100 Beech Forest Road\"         Laurel       MD 20708  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/patuxent     Patuxent  0"
  # echo "$0 \"13990 Parkeast Circle\"         Chantilly    VA 20151  2014-01-01 2014-12-31 ~/projects/daylight_sparse_05/agilex       Agilex    0"
  echo "$0 \"5155 Parkstone Drive\"            Chantilly    VA 20151  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/agilex       Agilex    0"
  echo "$0 \"6350 Walker Lane\"                Alexandria   VA 22310  2015-01-01 2015-12-31 ~/projects/daylight_sparse_05/cbp          CBP       0"
  echo "$0 \"2341 Middle Creek Lane\"          Reston       VA 20191  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/hamlet       Hamlet    0"
  echo "$0 \"2100 Reston Parkway\"             Reston       VA 20191  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/applied      AppliedSecurity 0"
  echo "$0 \"2100 Reston Parkway\"             Reston       VA 20191  2016-01-01 2016-12-31 \"/Users/gregcoats/Box Sync/box_docs/daylight_sparse_06/applied\" AppliedSecurity 0"
  echo "$0 \"12900 Worldgate Drive\"           Herndon      VA 20170  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/aws          AWS           0"
  echo "$0 \"9420 Key West Avenue\"            Rockville    MD 20850  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/drfirst      DrFirst       0"
  echo "$0 \"22451 Shaw Road\"                 Sterling     VA 20166  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/canon        Canon         0"
  echo "$0 \"4100 North Fairfax Drive\"        Arlington    VA 22203  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/canonusa     CanonUSA      0"
  echo "$0 \"11503 Sunrise Valley Drive\"      Reston       VA 20191  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/fusiondental FusionDental  0"
  echo "$0 \"1024 Paul Drive\"                 Rockville    MD 20851  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/karlharger   KarlHarger    0"
  echo "$0 \"1761 Business Center Drive\"      Reston       VA 20190  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/pragmatics   Pragmatics    0"
  echo "$0 \"1931 US 90\"              \"DeFuniak Springs\" FL 32433  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/defuniak   \"DeFuniak Springs Airport\" 1"
  echo "$0 \"1931 US 90\"              \"DeFuniak Springs\" FL 32433  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/defuniak   DeFuniakSpringsAirport 1"
  echo "$0 \"1024 Paul Drive\"                 Rockville    MD 20851  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/karlharger   KarlHarger    0"
  echo "$0 \"4301 North Fairfax Drive\"        Arlington    VA 22203  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/bae          BAESystems    0"
  echo "$0 \"2341 Middle Creek Lane\"          Reston       VA 20191  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/spacetest    \"The Hamlet\"  0"
  echo "$0 \"11600 Sunrise Valley Drive\"      Reston       VA 20191  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/concept_solutions \"ConceptSolutions\"  0"
  echo "$0 \"100 F Street NE\"                 Washington   DC 20549  2016-01-01 2016-12-31 ~/projects/daylight_sparse_05/sec          SEC           0"
  
  echo "$0 \"2341 Middle Creek Lane\"          Reston       VA 20191  2017-01-01 2017-12-31 ~/projects/daylight_sparse_05/hamlet       Hamlet    0"
  echo "$0 \"213 Overlook Drive\"              Coatesville  PA 19320  2017-01-01 2017-12-31 ~/projects/daylight_sparse_05/alexander    Alexander 0"
  
  echo "$0 \"7914 Brompton Street\"            Springfield     VA 22151  2018-01-01 2018-12-31 ~/projects/daylight_sparse_05/templeman Templeman 0"
  echo "$0 \"1215 Townbrook Crossing\"         Charlottesville VA 22901  2018-01-01 2018-12-31 ~/projects/daylight_sparse_05/mcvey     McVey     0"
  echo "$0 \"11600 American Dream Way\"        Reston          VA 20190  2018-01-01 2018-12-31 ~/projects/daylight_sparse_05/fanniemae FannieMae 0"
  # echo "$0 \"21000 Atlantic Boulevard\"        Sterling        VA 20166  2018-01-01 2018-12-31 ./TBG TBG 0"
  echo "$0 \"1851 Alexander Bell Drive\"       Reston          VA 20191  2018-01-01 2018-12-31 TBG TheBuffaloGroup 0"
  echo "can not continue, exiting"
  exit 1
fi

# sed 's/ /%20/g' replace space with %20
STREETADDRESS=$1
STREETADDRESS_NO_SPACES=`echo "${STREETADDRESS}" | sed 's/ /%20/g'`
CITY=$2
CITY_NO_SPACES=`echo "${CITY}"            | sed 's/ /%20/g'`
STATE=$3
ZIP=$4
echo "${STREETADDRESS} ${CITY} ${STATE} ${ZIP}"
echo "${STREETADDRESS_NO_SPACES} ${CITY_NO_SPACES} ${STATE} ${ZIP}"

echo "Contacting https://geoservices.tamu.edu"
echo " Input:  ${STREETADDRESS} ${CITY} ${STATE} ${ZIP}"
echo " Output: Latitude and Longitude"
VALUES=`curl --progress-bar "https://geoservices.tamu.edu/Services/Geocode/WebService/GeocoderWebServiceHttpNonParsed_V04_01.aspx?streetAddress=${STREETADDRESS_NO_SPACES}&city=${CITY_NO_SPACES}&state=${STATE}&zip=${ZIP}&apikey=demo&format=XML&census=false&notStore=false&version=4.01"`
echo "End curl --progress-bar"
echo " "
echo "VALUES=${VALUES}"
# VALUES=`curl --silent "https://geoservices.tamu.edu/Services/Geocode/WebService/GeocoderWebServiceHttpNonParsed_V04_01.aspx?streetAddress=${STREETADDRESS_NO_SPACES}&city=${CITY_NO_SPACES}&state=${STATE}&zip=${ZIP}&apikey=demo&format=XML&census=false&notStore=false&version=4.01"`
# sed 1d          delete first line
# sed '$d'        delete last  line
### For Linux
LATANDLONG=`echo "${VALUES}" | awk '{gsub(/<Latitude>/,"\n<Latitude>");print}' | sed '1d' | awk '{gsub(/<Longitude>/,"\n<Longitude>");print}' | awk '{gsub(/<\/Longitude>/,"<\/Longitude>\n");print}' | sed '$d'`

echo "LATANDLONG=${LATANDLONG}"
echo "Line 98"
# exit 0
# echo "${LATANDLONG}" | sed 's/\//'

LATITUDE_XML=`echo "${LATANDLONG}"  | head -1`
LONGITUDE_XML=`echo "${LATANDLONG}" | tail -1`
### For Linux
echo "LATITUDE_XML=${LATITUDE_XML}"
echo "LONGITUDE_XML=${LONGITUDE_XML}"
echo "Line 107"
# exit 0
# echo "<Longitude>-77.3237678<\/Longitude>" | sed 's/\\//'
echo "<Longitude>-77.3237678<\/Longitude>" | sed 's/\\//'
echo "Line 111"
# exit 0
echo "Before calling sed to remove back slash"
### LONGITUDE_XML=`echo "${LONGITUDE_XML}" | sed 's/\\//'`
echo "Line 115"
echo "${LONGITUDE_XML}"
echo "${LONGITUDE_XML}" | sed 's/\\//'
echo "${LONGITUDE_XML}" | sed 's/\\//'
echo "LONG_WITH_OUT is next"
echo "1"
LONG_WITH_OUT1=`echo "${LONGITUDE_XML}" | sed 's/\\//'`
echo "LONG_WITH_OUT1=${LONG_WITH_OUT1}"

echo "2"
LONG_WITH_OUT2=`echo "${LONGITUDE_XML}" | tr -d "\\\"`
echo "LONG_WITH_OUT=${LONG_WITH_OUT2}"

echo "3"
LONG_WITH_OUT3=`echo "${LONGITUDE_XML}" | sed -E 's!\\/!/!g'`
echo "LONG_WITH_OUT=${LONG_WITH_OUT3}"

LONGITUDE_XML=${LONG_WITH_OUT2}
# exit 0
### tr -d "\\"`

echo "After  calling sed"
echo "LONGITUDE_XML=${LONGITUDE_XML}"
echo "Is the extra \ now gone?"
echo "Line 117"
# exit 0
# sleep 2
LATI=`echo "${LATITUDE_XML}"             | sed 's/<Latitude>//' | sed 's/<\/Latitude>//'`
LONG_WITH_MINUS=`echo "${LONGITUDE_XML}" | sed 's/<Longitude>//' | sed 's/<\/Longitude>//'`
### For Linux
echo "LONG_WITH_MINUS=${LONG_WITH_MINUS}"

# sed '1s/^.//'   delete first character, which for longitude is a minus sign
LONG=`echo "${LONG_WITH_MINUS}" | sed '1s/^.//'`

echo "LATI=${LATI}"
echo "LONG=${LONG}"
### For Linux
### exit 0

# LATI=${1}
# LONG=${2}
STAR=${5}
STOP=${6}
# plot    (sunrise, type="l", col="red", ylim=c(0,24), xlab="2017",         ylab="AM/PM Hours", xaxt="n", yaxt="n")
# plot    (sunrise, type="l", col="red", ylim=c(0,24), xlab="${PLOT_XLAB}", ylab="AM/PM Hours", xaxt="n", yaxt="n")

echo " "
echo "For R, ..."
echo "For R, automatically get the year to used in the plot command, with xlab"
PLOT_XLAB=`echo "${5}" | cut -b1-4`
echo "PLOT_XLAB=${PLOT_XLAB}"
# echo "PLOT_XLAB=${PLOT_XLAB}"
# plot    (sunrise, ="l", col="red", ylim=c(0,24), xlab="${PLOT_XLAB}", ylab="AM/PM Hours", xaxt="n", yaxt="n")
# echo "plot    (sunrise, type=\"l\", col=\"red\", ylim=c(0,24), xlab=\"2017\",         ylab=\"AM/PM Hours\", xaxt=\"n\", yaxt=\"n\")"
# echo "plot    (sunrise, type="l", col="red", ylim=c(0,24), xlab="${PLOT_XLAB}", ylab="AM/PM Hours", xaxt="n", yaxt="n")""
# plot    (sunrise, ="l", col="red", ylim=c(0,24), xlab="${PLOT_XLAB}", ylab="AM/PM Hours", xaxt="n", yaxt="n")
echo "plot    (sunrise, type=\"l\", col=\"red\", ylim=c(0,24), xlab=\"${PLOT_XLAB}\", ylab=\"AM/PM Hours\", xaxt=\"n\", yaxt=\"n\")"

DIRE=${7}
FILE=${8}
TIMEZONEADJUST=${9}

echo "LATI=${LATI} LONG=${LONG} STAR=${STAR} STOP=${STOP} DIRE=${DIRE} FILE=${FILE} TIMEZONEADJUST=${TIMEZONEADJUST}"
echo "DIRE=${DIRE} FILE=${FILE}"
# exit 0

# Check that specified output directory exists
if [ ! -d "${DIRE}" ]
then
  echo "DIRE=${DIRE}"
  echo "The specified output directory does not exist"
  #  echo "can not continue, exiting"
  echo "If you want to create the directory ${DIRE}"
  echo "then enter Y or y or 1: "
  read CONTINUE
  echo "CONTINUE=${CONTINUE}"
  if [[ ${CONTINUE} == "Y" || ${CONTINUE} == "y" || ${CONTINUE} == "1" ]]
  then
    echo "Creating ${DIRE}"
    # mkdir -p Creates intermediate directories as required.
    mkdir -p       ${DIRE}
    ls    -lT      ${DIRE}
    if [ ! -d "${DIRE}" ]
    then
      echo "DIRE=${DIRE}"
      echo "The specified output directory was not successfully created"
      echo "The specified output directory does not exist"
      echo "can not continue, exiting"
      exit 1
    else
      echo "successfully created directory ${DIRE}"
      echo "continuing"
    fi
  else
    echo "DIRE=${DIRE}"
    echo "The specified output directory was not created"
    echo "The specified output directory does not exist"
    echo "can not continue, exiting"
    exit 1
  fi
fi

### For Linux
### exit 0
# Check for a valid filename
# Since bash variables need to begin with an alpha character,
# check that the argument passed for FILE begins with an alpha character
UPPER=`echo "${FILE}" | cut -b1 | grep [A-Z]`
LOWER=`echo "${FILE}" | cut -b1 | grep [a-z]`
# echo "UPPER=${UPPER}"
# echo "LOWER=${LOWER}"
if [ ! -z ${UPPER} ]
then
  echo "first character of FILE is UPPER case alpha"
  echo "continuing"
elif [ ! -z ${LOWER} ]
then
  echo "first character of FILE is LOWER case alpha"
  echo "continuing"
else
  echo "FILE=${FILE}"
  echo "first character of FILE is NOT UPPER case alpha"
  echo "first character of FILE is NOT LOWER case alpha"
  echo "first character of FILE must be an alpha character, preferably upper case"
  echo "can not continue, exiting"
  exit 1
fi

echo "To compute sunrise and sunset, this bash shell script calls the executable sunrise_05."
echo "To create the executable sunrise_05, the C program sunrise_05.c was compiled under OS X version 10.8."

### For Linux
UNAME=`uname`
echo "UNAME=${UNAME}"
if [[ ${UNAME} == "Darwin" ]]
then
  
  # MacOSXVersion="`sw_vers | grep 'ProductVersion:' | grep -o '[0-9]*\.[0-9]*\.[0-9]*'`"
  OSXVERSION="`sw_vers | grep 'ProductVersion:' | awk '{print $2}'`"
  MAJOROSXVERSION=`echo "${OSXVERSION}" | cut -b1-4`
  
  # Need to update this for OS X 10.9
  #if [[ ${MAJOROSXVERSION} == "10.8" ]]
  #then
  #  echo "Since this computer is running OS X version ${OSXVERSION},"
  #  echo "it is reasonable to anticipate that sunrise_05 will succeed."
  #  echo "continuing"
  #else
  #  echo "Since this computer is running OS X version ${OSXVERSION},"
  #  echo "it is unlikely that sunrise_05 will work."
  #  echo "If sunrise_05 does not work, then compile sunrise_05.c, using"
  #  echo "make sunrise_05"
  #  check to see if the file make exists
  #  if [[ ! -f /usr/bin/make ]]
  #  then
  #    echo "The file /usr/bin/make does not exist"
  #    echo "make is part of Apple Xcode, that can be downloaded for free from"
  #    echo "http://developer.apple.com/xcode/"
  #  fi
  #  echo "exiting"
  #  sleep 0
  #  exit  1
  #fi
elif [[ ${UNAME} == "Linux" ]]
then
  echo "Running Linux"
else
  echo "uname is neither Darwin, nor Linux"
  echo "can not continue, exiting"
fi

# Latest release of R is no longer named R64.app, but instead is named R.app
# if [[ -f /Applications/R64.app/Contents/MacOS/R ]]
if [[ -f /Applications/R.app/Contents/MacOS/R ]]
then
  # echo "/Applications/R64.app/Contents/MacOS/R exists"
  echo "/Applications/R.app/Contents/MacOS/R exists"
  echo "continuing"
else
  # echo "/Applications/R64.app/Contents/MacOS/R does not exist"
  echo "/Applications/R.app/Contents/MacOS/R does not exist"
  echo "This bash shell script formats sunrise, sunset, and daylight times"
  echo "for display by the"
  echo "R open source project for statistical computing and graphics."
  echo "R for OS X can be downloaded from"
  echo "http://cran.r-project.org/bin/macosx/"
  echo "exiting"
  echo "For Linux, NOT exiting"
  # exit  1
fi

# need the fields to be delimited by a tab, not a space
# even though the echo statement below contains tabs,
# the result of running the echo echo statement is that the fields are delimited by space, not tab
# echo "echo "start_date	start_time	end_date	end_time	duration" | tr ' ' '\t'"
# a successful alternative is to
# initially, use space as delimiter, then use tr to convert space to tab
## echo "echo "start_date start_time end_date end_time duration" | tr ' ' '\t'"

echo "computing"
echo "echo "start_date start_time end_date end_time duration" | tr ' ' '\t'" | /bin/bash

# figure out the list of dates, that you want sunrise and sunset times for
# pipe to bash to run the script that output a date, with a location defined by a latitude, longitude
# sed  to add to the beginning of the line the name of the script that computes the time between two dates
# pipe to bash to run the script that computes the length of time between sunrise and sunset
# tr   to replace space with tab, so that the column delimiter is tab, not space

# sed 's/^/.\/time_zone_01.sh /' adds to beginning of Line
# sed "s|$| ${TIMEZONEADJUST}|"  adds to end       of line
# Note: for this sed command that includes specifying the environment variable TIMEZONEADJUST,
# instead of using sed s/ / /, using s| | |
# instead of using sed s' ',   using sed s " "

rm -f ${DIRE}/${FILE}.txt
# ./loop_through_dates_12.sh ${STAR} ${STOP} ${LATI} ${LONG} | /bin/bash | sed 's/^/.\/two_dates_28.sh /' | /bin/bash | tr ' ' '\t' > ${DIRE}/${FILE}.txt

# echo "Calling ./loop_through_dates_14.sh"
echo "Calling ./loop_through_dates_123.sh"
# echo "Calling ./time_zone_01.sh"
# echo "Calling ./time_zone_103.sh"
echo "Calling ./time_zone_105.sh"
# echo "Calling ./two_dates_28.sh"
### For Linux
echo "Calling ./two_dates_132.sh"
# ./loop_through_dates_12.sh ${STAR} ${STOP} ${LATI} ${LONG} \
# ./loop_through_dates_14.sh ${STAR} ${STOP} ${LATI} ${LONG} \
# ./loop_through_dates_115.sh ${STAR} ${STOP} ${LATI} ${LONG}
#  | sed 's/^/.\/time_zone_01.sh /' \
# ./loop_through_dates_115.sh ${STAR} ${STOP} ${LATI} ${LONG}
# | sed 's/^/.\/two_dates_28.sh /' \
# ./loop_through_dates_115.sh ${STAR} ${STOP} ${LATI} ${LONG} \

### For Linux debugging
### exit 0

./loop_through_dates_123.sh ${STAR} ${STOP} ${LATI} ${LONG} \
| /bin/bash                       \
| sed 's/^/.\/time_zone_105.sh /' \
| sed "s|$| ${TIMEZONEADJUST}|"   \
| sed 's/Sunrise//'               \
| sed 's/Sunset//'                \
| /bin/bash                       \
| sed 's/^/.\/two_dates_132.sh /' \
| /bin/bash                       \
| tr ' ' '\t' > ${DIRE}/${FILE}.txt
#####
echo "Line 362 of daylight_160.sh"
### exit 0

echo " "
echo "creating .r file"
rm -f ${DIRE}/${FILE}.r
# sed 1 delete first line
# sed 2 add to beginning of line
# sed 3 add to end of line
# sed 4 delete last character
# sed 5 delete last character
echo "processing sunrise"
echo -n "sunrise  <- c("  > ${DIRE}/${FILE}.r
sed '1d' ${DIRE}/${FILE}.txt | awk '{print $2}' | sed 's/^/.\/hour_decimal_06.sh /' | bash | sed 's/$/, /' | sed '$s/.$//' | sed '$s/.$//' | tr -d '\n' >> ${DIRE}/${FILE}.r
echo ")" >> ${DIRE}/${FILE}.r
echo "processing sunset"
echo -n "sunset   <- c(" >> ${DIRE}/${FILE}.r
sed '1d' ${DIRE}/${FILE}.txt | awk '{print $4}' | sed 's/^/.\/hour_decimal_06.sh /' | bash | sed 's/$/, /' | sed '$s/.$//' | sed '$s/.$//' | tr -d '\n' >> ${DIRE}/${FILE}.r
echo ")" >> ${DIRE}/${FILE}.r
echo "processing daylight"
echo -n "daylight <- c(" >> ${DIRE}/${FILE}.r
sed '1d' ${DIRE}/${FILE}.txt | awk '{print $5}' | sed 's/^/.\/hour_decimal_06.sh /' | bash | sed 's/$/, /' | sed '$s/.$//' | sed '$s/.$//' | tr -d '\n' >> ${DIRE}/${FILE}.r
echo ")" >> ${DIRE}/${FILE}.r

echo "Add labels to ${DIRE}/${FILE}.r"
# rm -f ${DIRE}/${FILE}_labels.r
echo "pdf (file=\"${DIRE}/${FILE}.pdf\", width=10, height=7.5)" >> ${DIRE}/${FILE}.r
# cat  template_labels_01.r >> ${DIRE}/${FILE}.r
# cat  template_labels_04.r >> ${DIRE}/${FILE}.r
# For 2016, a leap year
# cat  template_labels_05.r >> ${DIRE}/${FILE}.r
# For 2017, not a leap year
# cat  template_labels_06.r >> ${DIRE}/${FILE}.r
# Automatically get year to be used in plot command with xlab
# cat  template_labels_07.r >> ${DIRE}/${FILE}.r

#       plot (sunrise, type="l", col="red", ylim=c(0,24), xlab="${PLOT_XLAB}", ylab="AM/PM Hours", xaxt="n", yaxt="n")
# echo "plot (sunrise, type=\"l\", col=\"red\", ylim=c(0,24), xlab=\"${PLOT_XLAB}\", ylab=\"AM/PM Hours\", xaxt=\"n\", yaxt=\"n\")"
#       plot (sunrise, type="l", col="red", ylim=c(0,24), xlab="${PLOT_XLAB}", ylab="AM/PM Hours", xaxt="n", yaxt="n")
# Append the echo plot to the existing .r file, where sunrise, sunset, and daylight are already defined
# This passes only the R plot command
echo "plot (sunrise, type=\"l\", col=\"red\", ylim=c(0,24), xlab=\"${PLOT_XLAB}\", ylab=\"AM/PM Hours\", xaxt=\"n\", yaxt=\"n\")" >> ${DIRE}/${FILE}.r
# Now append a bunch more stuff for the plot to the .r file
cat  template_labels_07.r >> ${DIRE}/${FILE}.r

echo "mtext (\"${FILE}, ${STREETADDRESS}, ${CITY}, ${STATE}, ${ZIP}\", 3, line=0, col=\"black\", cex=1.350)" >> ${DIRE}/${FILE}.r
echo "dev.off ()"       >> ${DIRE}/${FILE}.r
echo ""                 >> ${DIRE}/${FILE}.r

# echo "open -a BBEdit  ${DIRE}/${FILE}.r"

rm -f           ${DIRE}/${FILE}.pdf
# For daylight_48.sh, changed from /usr/bin/Rscript to /usr/local/bin/Rscript
# Before continuing, check that /usr/bin/Rscript exists
# Before continuing, check that /usr/local/bin/Rscript exists
# if [[ ! -f /usr/bin/Rscript ]]
if [[ ! -f /usr/local/bin/Rscript ]]
then
  # echo "The file /usr/bin/Rscript does not exist"
  echo "The file /usr/local/bin/Rscript does not exist"
  echo "Perhaps R has not yet been installed."
  echo "The R Project for Statistical Computing is at"
  echo "http://www.r-project.org"
  echo "can not continue, exiting"
  exit 1
fi
# echo "/usr/bin/Rscript       ${DIRE}/${FILE}.r"
echo "/usr/local/bin/Rscript ${DIRE}/${FILE}.r"
#       /usr/bin/Rscript       ${DIRE}/${FILE}.r
/usr/local/bin/Rscript ${DIRE}/${FILE}.r
ls -lT          ${DIRE}/${FILE}.txt
ls -lT          ${DIRE}/${FILE}.r
ls -lT          ${DIRE}/${FILE}.pdf
open -a Preview ${DIRE}/${FILE}.pdf

exit 0
