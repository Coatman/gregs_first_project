#! /bin/bash

# Loop through a series of dates
# Input  Start date, End date, Latitude, Longitude
# Output For one date: Call to ./sunrise_05 to calculate Sunrise, and Sunset
# 2018-07-05 Greg Coats

# Looping Through Dates
# http://glatter-gotz.com/blog/2011/02/19/looping-through-dates-in-a-bash-script-on-osx/

NUM_CLA="4"
if [ $# != ${NUM_CLA} ]
then
  echo "The number of command line arguments must be ${NUM_CLA}, but is $#"
  echo "To use this script, enter"
  echo "$0 <start_date> <end_date> <LATI> <LONG>"
  echo "For example"
  echo "$0 2013-01-14 2013-01-16 38.9328900 77.3535163"
  exit 0
fi

# Check the environment
UNAME=`uname`
echo "UNAME=${UNAME}"
if [ ${UNAME} = "Darwin" ]
then
  echo "For Darwin, use instead loop_through_dates_14.sh"
  echo "can not continue, exiting"
  exit 0
elif [ ${UNAME} = "Linux" ]
then
  echo "Proceeding"
else
  echo "Anticipated uname to be either Darwin or Linux, but is neither"
  echo "can not continue, exiting"
  exit 0
fi

# Use date -d with "+%s" to get the number of seconds since 1970-01-01 00:00:00
# Then, to determine if the while loop should continue, compare the current date seconds to the end date seconds

#### echo "CURRENT = begin"
# Set the CURRENT (= begin) date
####                  echo "$1" | sed 's/-//g'
CURRENT_DATE_YMD=`echo "$1" | sed 's/-//g'`
echo -n "CURRENT_DATE_YMD=${CURRENT_DATE_YMD}  "
#####                  echo "(date --date='${CURRENT_DATE_YMD}' \"+%s\")" | /bin/bash
CURRENT_DATE_SEC=`echo "(date --date='${CURRENT_DATE_YMD}' \"+%s\")" | /bin/bash`
# DATE_FOR_COMPUTATION=`echo "(date --date='@${CURRENT_DATE_SEC}' \"+%Y %m %d\")" | /bin/bash`
# CURRENT_DATE_SEC=`(date -d 20130114 "+%s")`
# CURRENT_DATE_SEC=`(date -d ${END_DATE_YMD})`
echo "CURRENT_DATE_SEC=${CURRENT_DATE_SEC}"

#### echo "END"
# Set the END date
####              echo "$2" | sed 's/-//g'
END_DATE_YMD=`echo "$2" | sed 's/-//g'`
echo -n "    END_DATE_YMD=${END_DATE_YMD}  "
END_DATE_SEC=`echo "(date --date='${END_DATE_YMD}' \"+%s\")" | /bin/bash`
#  END_DATE_SEC=`(date -d 20130116 "+%s")`
#  END_DATE_SEC=`(date -d ${END_DATE_YMD})`
echo "    END_DATE_SEC=${END_DATE_SEC}"

# 60 seconds/minute * 60 minutes/hour * 24 hours/day = 86400 seconds/day
OFFSET=86400

LATI=$3
LONG=$4
echo "LATI=${LATI}"
echo "LONG=${LONG}"
GET_SUNRISE_SUNSET="./sunrise_05"
GET_SUNRISE_SUNSET="./sunrise_linux.o"
# Begin processing the loop
while [ "${CURRENT_DATE_SEC}" -le "${END_DATE_SEC}" ]
do
  # The Linux date command is not aware of defined variables
  # so the following does not work, and instead yields an error message
  # (date --date='@${CURRENT_DATE_SEC}' \"+%Y %m %d\")"
  # date: invalid date
  # For Linux date command, the following includes an environemnt defined variable, and does work
  DATE_FOR_COMPUTATION=`echo "(date --date='@${CURRENT_DATE_SEC}' \"+%Y %m %d\")" | /bin/bash`
  echo "${GET_SUNRISE_SUNSET} ${DATE_FOR_COMPUTATION} ${LATI} ${LONG}"
  # Update date from today to tomorrow
  CURRENT_DATE_SEC=`echo "${CURRENT_DATE_SEC}+${OFFSET}" | bc`
  ### echo "CURRENT_DATE_SEC=${CURRENT_DATE_SEC}"
done

exit 0

