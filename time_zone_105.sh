#! /bin/bash

# Compute difference between two dates and times
# No longer format this time difference into days, hours, minutes, seconds
# Format time difference output into HH:MM:SS
# http://bashscripts.org/forum/viewtopic.php?f=7&t=76

# BEGIN{printf "%6.3f", y}
# BEGIN{printf "%6.4f", y}
# BEGIN{printf "%8.5f", y}
# BEGIN{printf "%7.5f", y}
# 2014-04-22 Greg Coats

NUM_CLA="5"
if [ $# != ${NUM_CLA} ]
then
  echo "The number of command line arguments must be ${NUM_CLA}, but is $#"
  echo "To use this script, enter ${NUM_CLA} command line arguments"
  #  echo "$0 <yyyy1> <mm1> <dd1> <hh1> <mm1> <ss1> <yyyy2> <mm2> <dd2> <hh2> <mm2> <ss2>"
  echo "For example"
  # 42506 08/14/12 20:45:00  2 0 0 1 5535 0 adrian.hjf.org nsrd savegroup info: starting diurnal
  # 38758 08/14/12 21:21:14  2 0 0 1 5535 0 adrian.hjf.org nsrd savegroup notice: diurnal completed
  #  echo "$0 07/31/12 05:15:00 07/31/12 06:01:48"
  #  year is now input as 4 digits, not 2 digits
  #  year is now first, not after day
  # sample input $ ./two_dates_28.sh 2012/07/15 22:11:33 2012/07/15 23:59:59
  # sample output  2012-07-15 22:11:33 2012-07-15 23:59:59 01:48:26
  echo "$0 YYYY/MM/DD HH:MM:SS YYYY/MM/DD HH:MM:SS TIMEZONEADJUST"
  # echo "$0 2012/07/15 22:11:33 2012/07/15 23:59:59 3"
  echo "$0 2012/07/15 12:15:25 2012/07/15 23:35:45 3"
  # echo "For testing, of when the back up begins before midnight, and the backup ends after midnight"
  # echo "$0 07/15/12 22:11:33 07/16/12 00:44:55"
  # echo the command line
  # echo "$0 $@"
  echo "can not continue, exiting"
  exit 1
fi

### echo "${0} ${1} ${2} ${3} ${4}"
# ./time_zone_104.sh   01-02-2018  07:28:11    01-02-2018  16:58:47 0
export YR1=`echo "${1}" | cut -b1-4`
export MONTH1=`echo "${1}" | cut -b6-7`
export DAY1=`echo "${1}" | cut -b9-10`
# export CENTURY1="20"
# export YEAR1=`echo "${CENTURY1}${YR1}"`
export YEAR1=`echo "${YR1}"`
export HOUR1=`echo "${2}" | cut -b1-2`
export MINUTE1=`echo "${2}" | cut -b4-5`
export SECOND1=`echo "${2}" | cut -b7-8`

## echo " "
### echo "YEAR1=${YEAR1} MONTH1=${MONTH1} DAY1=${DAY1} HOUR1=${HOUR1} MINUTE1=${MINUTE1} SECOND1=${SECOND1}"
export YEAR1=`echo   "${1}" | cut -b7-10`
## echo "Line 54"
export MONTH1=`echo  "${1}" | cut -b1-2`
## echo "Line 56"
export DAY1=`echo    "${1}" | cut -b4-5`
## echo "Line 58"
export HOUR1=`echo   "${2}" | cut -b1-2`
## echo "Line 60"
export MINUTE1=`echo "${2}" | cut -b4-5`
## echo "Line 62"
export SECOND1=`echo "${2}" | cut -b7-8`
## echo "Line 64"
### echo "YEAR1=${YEAR1} MONTH1=${MONTH1} DAY1=${DAY1} HOUR1=${HOUR1} MINUTE1=${MINUTE1} SECOND1=${SECOND1}"

## exit 0

export YR2=`echo "${3}" | cut -b1-4`
export MONTH2=`echo "${3}" | cut -b6-7`
export DAY2=`echo "${3}" | cut -b9-10`
# export CENTURY2="20"
# export YEAR2=`echo "${CENTURY2}${YR2}"`
export YEAR2=`echo "${YR2}"`
export HOUR2=`echo "${4}" | cut -b1-2`
export MINUTE2=`echo "${4}" | cut -b4-5`
export SECOND2=`echo "${4}" | cut -b7-8`

### echo "YEAR2=${YEAR2} MONTH2=${MONTH2} DAY2=${DAY2} HOUR2=${HOUR2} MINUTE2=${MINUTE2} SECOND2=${SECOND2}"
export YEAR2=`echo   "${3}" | cut -b7-10`
export MONTH2=`echo  "${3}" | cut -b1-2`
export DAY2=`echo    "${3}" | cut -b4-5`
export HOUR2=`echo   "${4}" | cut -b1-2`
export MINUTE2=`echo "${4}" | cut -b4-5`
export SECOND2=`echo "${4}" | cut -b7-8`
### echo "YEAR2=${YEAR2} MONTH2=${MONTH2} DAY2=${DAY2} HOUR2=${HOUR2} MINUTE2=${MINUTE2} SECOND2=${SECOND2}"


export TIMEZONEADJUST=${5}
## echo  "TIMEZONEADJUST=${TIMEZONEADJUST}"
# Need to avoid DAY1 and DAY2 being 08 and 09, which are interpreted as (can not exist) Octal 8 and Octal 9
# DAY1
export DAY1_MSB=`echo ${DAY1} | cut -b1`
export DAY1_LSB=`echo ${DAY1} | cut -b2`
# echo "DAY1_MSB=${DAY1_MSB} DAY1_LSB=${DAY1_LSB}"
if [[ ${DAY1_MSB} == 0 ]]
then
  let DAY1_NUM=${DAY1_LSB}
else
  let DAY1_NUM=${DAY1}
fi
### echo "DAY1_MSB=${DAY1_MSB} DAY1_LSB=${DAY1_LSB} DAY1_NUM=${DAY1_NUM}"

# DAY2
export DAY2_MSB=`echo ${DAY2} | cut -b1`
export DAY2_LSB=`echo ${DAY2} | cut -b2`
# echo "DAY2_MSB=${DAY2_MSB} DAY2_LSB=${DAY2_LSB}"
if [[ ${DAY2_MSB} == 0 ]]
then
  let DAY2_NUM=${DAY2_LSB}
else
  let DAY2_NUM=${DAY2}
fi
### echo "DAY2_MSB=${DAY2_MSB} DAY2_LSB=${DAY2_LSB} DAY2_NUM=${DAY2_NUM}"

let DIFF_DAY_NUM=DAY2_NUM-DAY1_NUM
### echo "DAY1_NUM=${DAY1_NUM} DAY2_NUM=${DAY2_NUM} DIFF_DAY_NUM=${DIFF_DAY_NUM}"

if [[ (${YEAR1} == ${YEAR2}) && (${MONTH1} == ${MONTH2}) && (${DAY1} == ${DAY2}) ]]
then
  # echo "begin year and end year, and begin month and end month, and begin day and end day are the same"
  # echo "OK to proceed"
  # Need to put something here, that is not commented out
  let FORWARD=1
elif [[ (${YEAR1} == ${YEAR2}) && (${MONTH1} == ${MONTH2}) && (${DIFF_DAY_NUM} == 1) ]]
then
  echo "begin year and end year, and begin month and end month are the same"
  echo "end day is 1 more than begin day"
  # echo "This may begin before midnight, and but after midnight"
  echo "when the end day = begin day +1"
  echo "possible alternative input, instead of this one input"
  echo "${1} ${2} ${3} ${4}"
  echo "try using these two inputs"
  echo "${MONTH1}/${DAY1}/${YR1} ${HOUR1}:${MINUTE1}:${SECOND1} ${MONTH1}/${DAY1}/${YR1} 23:59:59"
  echo "${MONTH1}/${DAY2}/${YR1} 00:00:00 ${MONTH1}/${DAY2}/${YR1} ${HOUR2}:${MINUTE2}:${SECOND2}"
  echo "can not continue, exiting"
  exit 1
else
  # echo "other conditions to accommodate"
  echo "perhaps the end day is > begin day +1"
  echo "perhaps the begin month and end month are not equal"
  echo "perhaps the begin year  and end year  are not equal"
  echo "can not continue, exiting"
  exit 1
fi

# EPOCH1=`date -j -v${YEAR1}y -v${MONTH1}m -v${DAY1}d -v${HOUR1}H -v${MINUTE1}M -v${SECOND1}S "+%s"`
# EPOCH2=`date -j -v${YEAR2}y -v${MONTH2}m -v${DAY2}d -v${HOUR2}H -v${MINUTE2}M -v${SECOND2}S "+%s"`
### currentDateTs=$(date    -d $1 "+%s")`
### Change for Linux
# EPOCH1=`date -j -v${YEAR1}y -v${MONTH1}m -v${DAY1}d -v${HOUR1}H -v${MINUTE1}M -v${SECOND1}S "+%s"`
# EPOCH2=`date -j -v${YEAR2}y -v${MONTH2}m -v${DAY2}d -v${HOUR2}H -v${MINUTE2}M -v${SECOND2}S "+%s"`
# EPOCH1=`date -d ${YEAR1}${MONTH1}${DAY1}${HOUR1}${MINUTE1}${SECOND1} "+%s"`
# EPOCH2=`date -d ${YEAR2}${MONTH2}${DAY2}${HOUR2}${MINUTE2}${SECOND2} "+%s"`


### echo "Processing Linux date input format"
### date --date=2000-12-20T10:01:12 "+%s"

# vim linux_date_input_formats_01.txt
# ubuntu$ date --date=2000-12-20T10:01:12 "+%s"
# 977324472
# Combined date and time of day items:          1972-09-24T20:02:00,000000-0500.

# http://www.gnu.org/software/coreutils/manual/html_node/Date-input-formats.html#Date-input-formats

# Combined date and time of day items:          1972-09-24T20:02:00,000000-0500.
# ubuntu$ date --date=2000-12-20T10:01:12 "+%s"
# 977324472

DATE_INPUT1=${YEAR1}-${MONTH1}-${DAY1}T${HOUR1}:${MINUTE1}:${SECOND1}
DATE_INPUT2=${YEAR2}-${MONTH2}-${DAY2}T${HOUR2}:${MINUTE2}:${SECOND2}

### echo "DATE_INPUT1=${DATE_INPUT1}"
### echo "DATE_INPUT2=${DATE_INPUT2}"

EPOCH1=`echo "(date --date='${DATE_INPUT1}' \"+%s\")" | /bin/bash`
EPOCH2=`echo "(date --date='${DATE_INPUT2}' \"+%s\")" | /bin/bash`
### echo "EPOCH1=${EPOCH1}"
### echo "EPOCH2=${EPOCH2}"


# This next statement (1-2) results in negative values
# DIFF_EPOCH=`echo "scale=0 ; (${EPOCH1} - ${EPOCH2})" | bc`
DIFF_EPOCH=`echo "scale=0 ; (${EPOCH2} - ${EPOCH1})" | bc`
# echo "DIFF_EPOCH=${DIFF_EPOCH}"

# TICS=`echo "${HOUR_DECIMAL} *(60 *60)" | bc`
TICS=${DIFF_EPOCH}
HOURS=`echo "(${TICS} /60 /60)" | bc`
HOURS02=`awk -v y=${HOURS} 'BEGIN{printf "%02d", y}'`

MINUTES=`echo "( (${TICS} /60) - (${TICS} /60 /60)*60)" | bc`
MINUTES02=`awk -v y=${MINUTES} 'BEGIN{printf "%02d", y}'`

# SECSSSS=`echo "${TICS} -(${HOURS02} *60*60) -(${MINUTES02} *60)" | bc`
# Adding 0.5 to minimize rounding error
SECSSSS=`echo "${TICS} -(${HOURS02} *60*60) -(${MINUTES02} *60) +0.5" | bc`
SECSSSS02=`awk -v y=${SECSSSS} 'BEGIN{printf "%02d", y}'`

# echo "${HOURS02}:${MINUTES02}:${SECSSSS02}"
# echo "${YEAR1}-${MONTH1}-${DAY1} ${HOUR1}:${MINUTE1}:${SECOND1} ${YEAR2}-${MONTH2}-${DAY2} ${HOUR2}:${MINUTE2}:${SECOND2} ${HOURS02}:${MINUTES02}:${SECSSSS02}"
HOUR1ADJUST=`echo "${HOUR1}-${TIMEZONEADJUST}" | bc`
HOUR2ADJUST=`echo "${HOUR2}-${TIMEZONEADJUST}" | bc`
HOUR1ADJUST2DIGITS=`awk -v y=${HOUR1ADJUST} 'BEGIN{printf "%02d", y}'`
HOUR2ADJUST2DIGITS=`awk -v y=${HOUR2ADJUST} 'BEGIN{printf "%02d", y}'`

## echo "${YEAR1}-${MONTH1}-${DAY1} ${HOUR1}:${MINUTE1}:${SECOND1} ${YEAR2}-${MONTH2}-${DAY2} ${HOUR2}:${MINUTE2}:${SECOND2}"
echo "${YEAR1}-${MONTH1}-${DAY1} ${HOUR1ADJUST2DIGITS}:${MINUTE1}:${SECOND1} ${YEAR2}-${MONTH2}-${DAY2} ${HOUR2ADJUST2DIGITS}:${MINUTE2}:${SECOND2}"

exit 0
