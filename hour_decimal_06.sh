#! /bin/bash

# Input  HH:MM:SS 12:34:56
# Output HH.HHHHH 12.58221
# 2013-03-13 Greg Coats

NUM_CLA="1"
if [ $# != ${NUM_CLA} ]
then
  echo "The number of command line arguments must be ${NUM_CLA}, but is $#"
  echo "To use this script, enter ${NUM_CLA} command line arguments"
  echo "For example"
  echo "$0 12:34:56"
  echo "can not continue, exiting"
  exit 1
fi

# sed delete last character
# sed '$s/.$//'
#                 echo "${1}" | sed '$s/.$//' | wc -c | awk '{print $1}'
WORD_COUNT_BYTES=`echo "${1}" | sed '$s/.$//' | wc -c | awk '{print $1}'`
# echo "WORD_COUNT_BYTES=${WORD_COUNT_BYTES}"
if [[ ${WORD_COUNT_BYTES} != "8" ]]
then
  echo "command line argument 1=${1}"
  echo "WORD_COUNT_BYTES=${WORD_COUNT_BYTES}"
  echo "WORD_COUNT_BYTES anticipated to be 8"
  echo "can not continue, exiting"
  exit 1
fi

export HOUR=`echo "${1}" | cut -b1-2`
export MINU=`echo "${1}" | cut -b4-5`
export SECO=`echo "${1}" | cut -b7-8`

#                            ${HOUR} + (${MINU}/60) + (${SECO}/(60*60))
#             echo "scale=5; ${HOUR} + (${MINU}/60) + (${SECO}/(60*60))" | bc
HOUR_DECIMAL=`echo "scale=5; ${HOUR} + (${MINU}/60) + (${SECO}/(60*60))" | bc`
# echo "HOUR_DECIMAL=${HOUR_DECIMAL}"
echo "${HOUR_DECIMAL}"

exit 0
